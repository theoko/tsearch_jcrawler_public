/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.uci.ics.crawler4j.parser;

import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import edu.uci.ics.crawler4j.url.WebURL;

public class HtmlParseData implements ParseData {

    private String html;
    private String text;
    private String title;
    private Document doc;
    private Map<String, String> metaTags;

    private Set<WebURL> outgoingUrls;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, String> getMetaTags() {
        return metaTags;
    }

    public void setMetaTags(Map<String, String> metaTags) {
        this.metaTags = metaTags;
    }

    @Override
    public Set<WebURL> getOutgoingUrls() {
        return outgoingUrls;
    }

    @Override
    public void setOutgoingUrls(Set<WebURL> outgoingUrls) {
        this.outgoingUrls = outgoingUrls;
    }

    @Override
    public String toString() {
        return text;
    }
    
    public String getBodyTitle() {
    	doc = Jsoup.parse(getHtml(), "UTF-8");
    	String title = doc.title();
    	
    	return title;
    }
    
    public String getBodyText() {
    	doc = Jsoup.parse(getHtml(), "UTF-8");
    	String text = doc.body().text();
    	
    	return text;
    }
    
    public String getDescription() {
    	doc = Jsoup.parse(getHtml(), "UTF-8");
    	Element metalinks = doc.select("meta[name*=description]").first();
    	
    	try {
    		String desc = metalinks.attr("content").toString();
	    	if (desc != null) {
	    		return desc;
	    	} else {
	    		return "";
	    	}
    	} catch (Exception e) {
    		return "";
    	}
    }
    
    public String getKeywords() {
    	doc = Jsoup.parse(getHtml(), "UTF-8");
    	Element metalinks = doc.select("meta[name*=keywords]").first();
    	
    	try {
    		String keywords = metalinks.attr("content").toString();
	    	if (keywords != null) {
	    		return keywords;
	    	} else {
	    		return "";
	    	}
    	} catch (Exception e) {
    		return "";
    	}
    }
    
    public String getImage() {
    	doc = Jsoup.parse(getHtml(), "UTF-8");
    	Element metalinks = doc.select("meta[name*=image]").first();
    	
    	try {
    		String image = metalinks.attr("content").toString();
	    	if (image != null) {
	    		return image;
	    	} else {
	    		return "";
	    	}
    	} catch (Exception e) {
    		return "";
    	}
    }
}