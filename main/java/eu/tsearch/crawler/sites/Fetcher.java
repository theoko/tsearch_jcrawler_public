package eu.tsearch.crawler.sites;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.tsearch.crawler.BotController;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class Fetcher {
	private static final Logger logger = LoggerFactory.getLogger(BotController.class);
	
	public Fetcher(String alexaURL) {
		File f = new File("tmp");
		if(!f.exists()) { 
		    try {
		    	f.mkdir();
		    } catch(Exception e) {
		    	logger.error("Error creating directory: {}", e.getMessage());
		    }
		    downloadSites(alexaURL);
		} else {
			File csv = new File("tmp/top-1m.csv");
			if(!csv.exists()) {
				downloadSites(alexaURL);
			}
		}
	}
	
	private void downloadSites(String url) {
		URL website;
		try {
			website = new URL(url);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			@SuppressWarnings("resource")
			FileOutputStream fos = new FileOutputStream("tmp/top-1m.zip");
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
	         ZipFile zipFile = new ZipFile("tmp/top-1m.zip");
	         zipFile.extractAll("tmp");
	    } catch (ZipException e) {
	        e.printStackTrace();
	    }
	}
	
	public String[][] getSitesInArray(int numOfMaster) {
        String csvFile = "tmp/top-1m.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        String[] sites = new String[1000000];
        int splitter = 1000000 / numOfMaster;
        
        try {

            br = new BufferedReader(new FileReader(csvFile));
            int i=0;
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] site = line.split(cvsSplitBy);
                sites[i] = "http://www." + site[1]; 
                
                i++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                    return divideArray(sites, splitter);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
	}
	
	private static String[][] divideArray(String[] sites, int chunksize) {
        String[][] ret = new String[(int)Math.ceil(sites.length / (double)chunksize)][chunksize];

        int start = 0;

        for(int i = 0; i < ret.length; i++) {
            ret[i] = Arrays.copyOfRange(sites,start, start + chunksize);
            start += chunksize ;
        }

        return ret;
    }
}
