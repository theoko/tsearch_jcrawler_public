package eu.tsearch.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import eu.tsearch.crawler.sites.Fetcher;

public class Init {
	private static final Logger logger = LoggerFactory.getLogger(BotController.class);
	private static Database db;
	public static final String alexaURL = "https://s3.amazonaws.com/alexa-static/top-1m.csv.zip";
	
	public static void main(String[] arguments) {
		if (arguments.length != 3) {
            logger.info("Needed parameters: ");
            logger.info("\t rootFolder (it will contain intermediate crawl data)");
            logger.info("\t numberOfMasterCralwers (number of concurrent master)");
            logger.info("\t numberOfCralwers (number of concurrent threads)");
            return;
        }
		
        db = new Database();
    	db.createIndexes();
    	
    	String crawlStorageFolder = arguments[0];
    	int numberOfMasterCrawlers = Integer.parseInt(arguments[1]);
        int numberOfCrawlers = Integer.parseInt(arguments[2]);
        
		Fetcher sites = new Fetcher(alexaURL);
		String[][] domains = sites.getSitesInArray(numberOfMasterCrawlers);
		
        CrawlConfig[] config = new CrawlConfig[numberOfMasterCrawlers];
        
        String userAgentString = "Mozilla/5.0 (compatible; tSearchBot/0.1; +http://tsearch.eu/bot)";
        
        for(int i=0;i<config.length;i++) {
        	config[i] = new CrawlConfig();
	        config[i].setCrawlStorageFolder(crawlStorageFolder + "/crawler" + i);
	        config[i].setUserAgentString(userAgentString);
	        
	        config[i].setPolitenessDelay(100);
	        config[i].setMaxDepthOfCrawling(5);
	        
	        config[i].setMaxPagesToFetch(-1);
	        
	        config[i].setIncludeBinaryContentInCrawling(false);
	        
	        config[i].setResumableCrawling(false);
        }
        
        PageFetcher[] pageFetcher = new PageFetcher[numberOfMasterCrawlers];
        for(int i=0;i<pageFetcher.length;i++) {
        	pageFetcher[i] = new PageFetcher(config[i]);
        }
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        robotstxtConfig.setEnabled(false);
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher[0]);
		try {
			CrawlController[] controller = new CrawlController[numberOfMasterCrawlers];
			for(int i=0;i<controller.length;i++) {
				controller[i] = new CrawlController(config[i], pageFetcher[i], robotstxtServer);
				if(null != domains) {
					
						for(int j=0;j<domains[i].length;j++) {
							try {
								controller[i].setCustomData(domains[i]);
								controller[i].addSeed(domains[i][j]);
							} catch(Exception e) {
								logger.warn("Exception(3): {}", e.getMessage());
							}
						}
					
					controller[i].startNonBlocking(MultiBot.class, numberOfCrawlers);
					logger.info("controller {} started!", i);
				} else {
					throw new Exception("File cannot be read!");
				}
			}
			for(int i=0;i<controller.length;i++) {
		        controller[i].waitUntilFinish();
		        logger.info("Crawler {} is finished.", i);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
