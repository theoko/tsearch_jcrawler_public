package eu.tsearch.crawler.news;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

//import org.apache.http.Header;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import eu.tsearch.crawler.Database;

public class NewsBot extends WebCrawler {
	
	private static final Pattern IMAGE_EXTENSIONS = Pattern.compile(
			".*(\\.(css|js|bmp|gif|jpe?g" + "|png|tiff?|mid|mp2|mp3|mp4" +
		            "|wav|avi|mov|mpeg|ram|m4v|pdf" + "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
	private Map<String,String> pageData;
	private Map<String, String> linksDB;
	private Map<String, String> cache;
	private Database db = new Database();

    /**
     * You should implement this function to specify whether the given url
     * should be crawled or not (based on your crawling logic).
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        // Ignore the url if it has an extension that matches our defined set of image extensions.
        if (IMAGE_EXTENSIONS.matcher(href).matches()) {
            return false;
        }
        return true;

        // Only accept the url if it is in the "www.ics.uci.edu" domain and protocol is "http".
//        String current = url.getURL();
//        boolean allowed = href.startsWith(current);
//        if(!allowed) {
//        	logger.debug("URL: {} was not allowed to be crawled!", current);
//        }
        
//        return allowed;
    }

    /**
     * This function is called when a page is fetched and ready to be processed
     * by your program.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    public void visit(Page page) {
    	pageData = new HashMap();
    	cache = new HashMap();
    	linksDB = new HashMap();
//        int docid = page.getWebURL().getDocid();
        String url = page.getWebURL().getURL();
        String domain = page.getWebURL().getDomain();
        String path = page.getWebURL().getPath();
        String subDomain = page.getWebURL().getSubDomain();
        String parentUrl = page.getWebURL().getParentUrl();
        String anchor = page.getWebURL().getAnchor();
        
        pageData.put("url", url);
        pageData.put("referer", parentUrl);
        
        cache.put("url", url);
        
        linksDB.put("url", url);
        linksDB.put("referer", parentUrl);
        
        linksDB.put("domain", domain);
        linksDB.put("path", path);
        linksDB.put("subdomain", subDomain);

//        logger.debug("Docid: {}", docid);
//        logger.info("URL: {}", url);
//        logger.debug("Domain: '{}'", domain);
//        logger.debug("Sub-domain: '{}'", subDomain);
//        logger.debug("Path: '{}'", path);
//        logger.debug("Parent page: {}", parentUrl);
//        logger.debug("Anchor text: {}", anchor);

        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
//            String text = htmlParseData.getText();
            String html = htmlParseData.getHtml();
            String bodyText = htmlParseData.getBodyText();
            String title = htmlParseData.getBodyTitle();
            String metaDescription = htmlParseData.getDescription();
            String metaKeywords = htmlParseData.getKeywords();
            String image = htmlParseData.getImage();
            Set<WebURL> links = htmlParseData.getOutgoingUrls();
            
            try {
	            pageData.put("text", bodyText);
	            pageData.put("title", title);
	            
	            pageData.put("description", metaDescription);
	            pageData.put("keywords", metaKeywords);
	            pageData.put("anchor", anchor);
	            pageData.put("image", image);
	            pageData.put("category", "News and Media");
	            pageData.put("lScore", Integer.toString(links.size()));
	            
	            linksDB.put("lScore", Integer.toString(links.size()));
	            linksDB.put("links", links.toString());

	            cache.put("html", html);
            } catch(Exception e) {
            	logger.warn(e.getMessage());
            }
//            logger.debug("Title: {}", title);
//            logger.debug("Meta Description: {}", metaDescription);
//            logger.debug("Meta Keywords: {}", metaKeywords);
//            logger.debug("Text length: {}", text.length());
//            logger.debug("Html length: {}", html.length());
//            logger.debug("Number of outgoing links: {}", links.size());
        }
        
        try {
            db.indexDoc(cache, "tsearch_sites_cache", "site");
        } catch(Exception e) {
        	logger.warn("Warning: {}", e.getMessage());
        }
        
        try {
            db.indexDoc(linksDB, "tsearch_links", "site");
        } catch(Exception e) {
        	logger.warn("Warning: {}", e.getMessage());
        }
        
        try {
        	db.indexDoc(pageData, "tsearch_sites", "site");
        } catch(Exception e) {
        	logger.warn("Warning: {}", e.getMessage());
        }
        
//        Header[] responseHeaders = page.getFetchResponseHeaders();
//        if (responseHeaders != null) {
//            logger.debug("Response headers:");
//            for (Header header : responseHeaders) {
//                logger.debug("\t{}: {}", header.getName(), header.getValue());
//            }
//        }

//        logger.debug("=============");
    }
    
}
