package eu.tsearch.crawler;

import java.io.IOException;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Index;
import io.searchbox.indices.CreateIndex;

public class Database {

	private static final Logger logger = LoggerFactory.getLogger(Database.class);
	private static final String host = "http://localhost:9200";
	private static String[] indexes = {"tsearch_sites", "tsearch_sites_cache", "tsearch_links"};
	public static JestClientFactory factory = new JestClientFactory();
	public static JestClient client;
	
	public Database() {
		try {
			factory.setHttpClientConfig(new HttpClientConfig
                    .Builder(host)
                    .multiThreaded(true)
                    .build());
			client = factory.getObject();
			
		} catch(Exception e) {
			System.out.println("Exception(1): " + e.getMessage());
		}
	}
	
	private static String hashString(String message, String algorithm)
	{
	 
	    try {
	        MessageDigest digest = MessageDigest.getInstance(algorithm);
	        byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));
	 
	        return convertByteArrayToHexString(hashedBytes);
	    } catch (Exception ex) {
	    	logger.error(ex.getMessage());
	    }
		return null;
	}
	
	private static String convertByteArrayToHexString(byte[] arrayBytes) {
	    StringBuffer stringBuffer = new StringBuffer();
	    for (int i = 0; i < arrayBytes.length; i++) {
	        stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
	                .substring(1));
	    }
	    return stringBuffer.toString();
	}
	
	public static String generateID(String message) {
	    return hashString(message, "MD5");
	}
	
	public void createIndexes() {
		try {
			for (int i = 0; i < indexes.length; i++) {
				logger.debug("Creating index '{}'", indexes[i]);
				client.execute(new CreateIndex.Builder(indexes[i]).build());
			}
		} catch(Exception e) {
			System.out.println("Exception(2): " + e.getMessage());
		}
	}

	public void indexDoc(Map<String,String> docData, String db, String type) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		docData.put("timestamp", timestamp.toString());
		try {
			if(docData.containsKey("url")) {
				Index index = new Index.Builder(docData).index(db).type(type).id(generateID(docData.get("url"))).build();
				client.execute(index);
			} else {
				Index index = new Index.Builder(docData).index(db).type(type).build();
				client.execute(index);
			}
		} catch (IOException e) {
			logger.error("Error: {}", e.getMessage());
		}
	}
	
	public void getDoc() {
		
	}

}
