package eu.tsearch.crawler.news;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import eu.tsearch.crawler.Database;

public class Init {
	private static final Logger logger = LoggerFactory.getLogger(Init.class);
	private static final String sURL="http://compute.tsearch.eu/category.php?category=News+and+Media";
	public static final String userAgentString = "Mozilla/5.0 (compatible; tSearchBot/0.1; +http://tsearch.eu/bot)";
	private static Database db;
//	public static String[][] domains;
	
	public static void main(String[] args) {
        if (args.length != 2) {
            logger.info("Needed parameters: ");
            logger.info("\t rootFolder (it will contain intermediate crawl data)");
            logger.info("\t numberOfCralwers (number of concurrent threads)");
            return;
        }
		
		db = new Database();
    	db.createIndexes();
    	
    	String crawlStorageFolder = args[0];
    	int numberOfCrawlers = Integer.parseInt(args[1]);
        CrawlConfig config = new CrawlConfig();
        
        config.setCrawlStorageFolder(crawlStorageFolder);
        config.setUserAgentString(userAgentString);
        config.setPolitenessDelay(100);
        config.setMaxDepthOfCrawling(10);
        config.setMaxPagesToFetch(-1);
        config.setIncludeBinaryContentInCrawling(false);
        config.setResumableCrawling(false);
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        robotstxtConfig.setEnabled(false);
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller;
		try {
			controller = new CrawlController(config, pageFetcher, robotstxtServer);
			
			while(true) {
				String json = readUrl(sURL);
				
				JSONArray jsonarray = new JSONArray(json);
				int i;
				for (i = 0; i < jsonarray.length(); i++) {
				    JSONObject jsonobject = jsonarray.getJSONObject(i);
				    String domain = jsonobject.getString("domain");
				    controller.addSeed("http://www."+domain);
				}
//				domains = new String[i][2];
//				for (i = 0; i < jsonarray.length(); i++) {
//					JSONObject jsonobject = jsonarray.getJSONObject(i);
//					String domain = jsonobject.getString("domain");
//					String category = jsonobject.getString("category");
//					domains[i][1] = "http://www."+domain;
//					domains[i][2] = category;
//				}
				logger.debug("Loaded {} news websites", i);
				logger.debug("Crawling...");
				controller.start(NewsBot.class, numberOfCrawlers);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static String readUrl(String urlString) {
	    try {
	    	URL url = new URL(urlString);
	    	URLConnection con = url.openConnection();
	    	con.addRequestProperty("User-Agent", userAgentString);
	    	InputStream in = con.getInputStream();
	    	String encoding = con.getContentEncoding();
	    	encoding = encoding == null ? "UTF-8" : encoding;
	    	String body = IOUtils.toString(in, encoding);

	        return body;
	    } catch(Exception e) {
	    	e.printStackTrace();
	    	return "";
	    }
	}
}
