package eu.tsearch.crawler;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.*;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import eu.tsearch.crawler.Bot;

public class BotController {
	
    private static final Logger logger = LoggerFactory.getLogger(BotController.class);
    private static Database db;
    private static final String TASK_QUEUE_NAME = "crawler";
    private static final String host = "localhost";
    private static final String rabbitUsername = "guest";
    private static final String rabbitPassword = "123";

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            logger.info("Needed parameters: ");
            logger.info("\t rootFolder (it will contain intermediate crawl data)");
            logger.info("\t numberOfCralwers (number of concurrent threads)");
            return;
        }
        
        db = new Database();
    	db.createIndexes();
        
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(rabbitUsername);
        factory.setPassword(rabbitPassword);
        factory.setHost(host);
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();
        
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        logger.debug(" [*] Waiting for messages.");
        
        channel.basicQos(1);
    
		        final Consumer consumer = new DefaultConsumer(channel) {
		        	
		        	@Override
		            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		              String message = new String(body, "UTF-8");
		
		              logger.debug(" [x] Received '" + message + "'");
		              try {
		            	  doCrawl(args, message);
		              } finally {
		                logger.debug(" [x] Done: {}", message);
		                channel.basicAck(envelope.getDeliveryTag(), false);
		              }
		            }
		          };
		          boolean autoAck = false;
		          channel.basicConsume(TASK_QUEUE_NAME, autoAck, consumer);
		          
            	  
        
        }
    
    private static void doCrawl(String[] args, String url) {
        /*
         * START OF WORK
         */

    /*
     * crawlStorageFolder is a folder where intermediate crawl data is
     * stored.
     */
        String crawlStorageFolder = args[0];

    /*
     * numberOfCrawlers shows the number of concurrent threads that should
     * be initiated for crawling.
     */
        int numberOfCrawlers = Integer.parseInt(args[1]);

        CrawlConfig config = new CrawlConfig();

        String userAgentString = "Mozilla/5.0 (compatible; tSearchBot/0.1; +http://tsearch.eu/bot)";
        config.setCrawlStorageFolder(crawlStorageFolder);
        config.setUserAgentString(userAgentString);

    /*
     * Be polite: Make sure that we don't send more than 1 request per
     * second (1000 milliseconds between requests).
     */
        config.setPolitenessDelay(100);

    /*
     * You can set the maximum crawl depth here. The default value is -1 for
     * unlimited depth
     */
        config.setMaxDepthOfCrawling(2);

    /*
     * You can set the maximum number of pages to crawl. The default value
     * is -1 for unlimited number of pages
     */
        config.setMaxPagesToFetch(-1);

        /**
         * Do you want crawler4j to crawl also binary data ?
         * example: the contents of pdf, or the metadata of images etc
         */
        config.setIncludeBinaryContentInCrawling(false);

    /*
     * Do you need to set a proxy? If so, you can use:
     * config.setProxyHost("proxyserver.example.com");
     * config.setProxyPort(8080);
     *
     * If your proxy also needs authentication:
     * config.setProxyUsername(username); config.getProxyPassword(password);
     */

    /*
     * This config parameter can be used to set your crawl to be resumable
     * (meaning that you can resume the crawl from a previously
     * interrupted/crashed crawl). Note: if you enable resuming feature and
     * want to start a fresh crawl, you need to delete the contents of
     * rootFolder manually.
     */
        config.setResumableCrawling(false);

    /*
     * Instantiate the controller for this crawl.
     */
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        robotstxtConfig.setEnabled(false);
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller;
		try {
			controller = new CrawlController(config, pageFetcher, robotstxtServer);
			
		    /*
		     * For each crawl, you need to add some seed urls. These are the first
		     * URLs that are fetched and then the crawler starts following links
		     * which are found in these pages
		     *
		     *   
		     *   controller.addSeed("http://www.ics.uci.edu/~lopes/");
		     *   controller.addSeed("http://www.ics.uci.edu/~welling/");
			 */
		        
		        controller.addSeed(url);
		        logger.debug("Adding: {}", url);
		        
		        /*
		  	     * Start the crawl. This is a blocking operation, meaning that your code
		  	     * will reach the line after this only when crawling is finished.
		  	     */
		  	        controller.start(Bot.class, numberOfCrawlers);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

}
